/**
  ******************************************************************************
  * @file    usart.h
  * @brief   This file contains all the function prototypes for
  *          the usart.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern UART_HandleTypeDef huart2;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_USART2_UART_Init(void);

/* USER CODE BEGIN Prototypes */
//!
//! \brief UsartPeriodic Периодическая функция передачика uart. Здесь мы мониторим размер буфера передачи
//!                      Когда он делается ненулевой (в результате вызова UsartTransmit), то мы выполняем
//!                      цикл передачи.
//!
void UsartPeriodic(void);


//!
//! \brief UsartTransmit Передать буфер по UART. Здесь мы копируем в буфер передачи
//!                      все символы из входного буфера, пока не встретится символ 0xa.
//!                      При этом подсчитываем количество символов в буфере
//! \param buf           Входной буфер
//!
void UsartTransmit( uint8_t *buf );

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __USART_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
