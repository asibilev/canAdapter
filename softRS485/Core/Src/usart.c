/**
  ******************************************************************************
  * @file    usart.c
  * @brief   This file provides code for the configuration
  *          of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
#include "gpio.h"
#include "usbd_cdc_if.h"
/* USER CODE END 0 */

UART_HandleTypeDef huart2;

/* USART2 init function */

void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* USART2 clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();

    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_3);

  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
static uint8_t txBuf[64];
static int     txSize;

static uint8_t rxBuf[64];
static int     rxSize;

static uint8_t ch8;

static int blink;

//!
//! \brief UsartPeriodic Периодическая функция передачика uart. Здесь мы мониторим размер буфера передачи
//!                      Когда он делается ненулевой (в результате вызова UsartTransmit), то мы выполняем
//!                      цикл передачи.
//!
void UsartPeriodic(void) {
  //Мигаем красным светодиодом
  if( blink < HAL_GetTick() ) {
    blink = HAL_GetTick() + 300;
    HAL_GPIO_TogglePin(ledRed_GPIO_Port, ledRed_Pin);
    }

  //Проверить, есть ли символы для передачи
  if( txSize ) {
    //Выставим разрешение передачи
    HAL_GPIO_WritePin( rs485en_GPIO_Port, rs485en_Pin, GPIO_PIN_SET );
    HAL_GPIO_WritePin(ledGreen_GPIO_Port, ledGreen_Pin, GPIO_PIN_SET);
    //Начать передачу
    HAL_UART_Transmit( &huart2, txBuf, txSize, HAL_MAX_DELAY );
    //Сбросить разрешение передачи
    HAL_GPIO_WritePin(ledGreen_GPIO_Port, ledGreen_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin( rs485en_GPIO_Port, rs485en_Pin, GPIO_PIN_RESET );
    //Фиктивное чтение из регистра приема, чтобы вычистить хлам, который образовался здесь из-за эха
    if( huart2.Instance->SR & ( UART_FLAG_IDLE | UART_FLAG_ORE | UART_FLAG_NE | UART_FLAG_FE | UART_FLAG_PE ) )
      __HAL_UART_FLUSH_DRREGISTER( &huart2 );
    if( huart2.Instance->SR & ( UART_FLAG_IDLE | UART_FLAG_ORE | UART_FLAG_NE | UART_FLAG_FE | UART_FLAG_PE ) )
      __HAL_UART_FLUSH_DRREGISTER( &huart2 );
    txSize = 0;
    }

  if( huart2.Instance->SR & ( UART_FLAG_IDLE | UART_FLAG_ORE | UART_FLAG_NE | UART_FLAG_FE | UART_FLAG_PE ) )
    __HAL_UART_FLUSH_DRREGISTER( &huart2 );

  //Проверить, есть ли принятые символы
  if( __HAL_UART_GET_FLAG( &huart2, UART_FLAG_RXNE) ) {
    //Есть очередной символ
    uint8_t ch = (uint8_t)(huart2.Instance->DR & (uint8_t)0x00FF);
    //Отбросим шлак
    if( ch ) {
      rxBuf[rxSize++] = ch;
      if( rxSize >= 62 || ch == 0xa ) {
        if( ch != 0xa ) {
          rxBuf[rxSize++] = 0xd;
          rxBuf[rxSize++] = 0xa;
          }
        CDC_Transmit_FS( rxBuf, rxSize );
        rxSize = 0;
        }
      }
    }
  }


//!
//! \brief UsartTransmit Передать буфер по UART. Здесь мы копируем в буфер передачи
//!                      все символы из входного буфера, пока не встретится символ 0xa.
//!                      При этом подсчитываем количество символов в буфере
//! \param buf           Входной буфер
//!
void UsartTransmit( uint8_t *buf ) {
  if( txSize == 0 && buf[0] == ':' ) {
    uint8_t ch = 0;
    while( txSize < 64 && ch != '\xa' )
      txBuf[txSize++] = ch = *buf++;
    }
  }

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
