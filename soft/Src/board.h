/*
   Проект "Управляющая программа can-адаптера, плата V1"
   Автор
     Сибилев А.С.
   Описание
     Интерфейс между hal ядром и основной программой
*/

#ifndef BOARD_H
#define BOARD_H

//Место для размещения текущих температур по каналам
extern int currentTemper[16];

//Место для температур из архива
extern int archiveTemper[16];

//Внутренние температуры, измеренные по термометрам в микросхемах АЦП
extern int internalTemper[4];

//Общая температура платы
extern int boardTemper;

//Количество каналов в регистраторе
extern int channelCount;

//Максимальное количество записей в архиве
extern int archiveMax;

#ifdef __cplusplus
//Раздел C++ описаний


extern "C" {
#endif
//Раздел C описаний

//!
//! \brief mainLoopProcess Функция вызываемая циклически в главном цикле
//!
void mainLoopProcess(void);


//!
//! \brief mainLoopInit Инициализация, необходимая для C++
//!
void mainLoopInit(void);

#ifdef __cplusplus
}
#endif

#endif // BOARD_H
