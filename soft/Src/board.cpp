/*
   Проект "Управляющая программа регистратора, плата V7"
   Автор
     Сибилев А.С.
   Описание
     Интерфейс между hal ядром и основной программой
*/
#include "config.h"
#include "board.h"
#include "sysTick.h"
#include "main.h"
#include "usbd_cdc_if.h"
//#include "SvTextStreamIn.h"
//#include "SvTextStreamOut.h"
//#include "../../../SaliShuttle/StComBook.h"

static int ledBlink;


class Out {
    uint8_t mBuf[64];
    int     mLen;
  public:


    void     init( char cmd ) {
      mBuf[0] = cmd;
      mLen = 1;
      }




    void     addChar( char ch ) {
      if( mLen < 62 ) {
        mBuf[mLen++] = ch;
        }
      }


    void     addString( const char *str ) {
      while( *str )
        addChar( *str++ );
      }



    void     addHex( int val ) {
      val &= 0xf;
      if( val < 10 ) addChar( '0' + val );
      else           addChar( 'A' + val - 10 );
      }



    void     addHex2( int val ) {
      addHex( val >> 4 );
      addHex( val );
      }


    void     addHex3( int val ) {
      addHex( val >> 8 );
      addHex2( val );
      }


    void     end() { mBuf[mLen++] = '\r'; mBuf[mLen] = 0; }

    int      lenght() const { return mLen; }

    uint8_t *buffer() { return mBuf; }
  };

static Out usbOut;

extern "C" {




//!
//! \brief mainLoopInit Инициализация, необходимая для C++
//!
void mainLoopInit(void)
  {
  }

//!
//! \brief mainLoopProcess Функция вызываемая циклически в главном цикле
//!
void mainLoopProcess(void)
  {
  //Мигаем светодиодом для обозначения процесса
  if( TestTimeOut(ledBlink) ) {
    ledBlink = svTickCount + 300;
    HAL_GPIO_TogglePin( ledGreen_GPIO_Port, ledGreen_Pin );
    }


  //Выполнить обработку команд usb
  //static SvTextStreamOut out;

  if( UserRxBufferFS[0] == 0 ) return;

  //Проверить наличие завершающего символа \n
  int len;
  for( len = 0; len < 64; len++ )
    if( UserRxBufferFS[len] == '\n' || UserRxBufferFS[len] == '\r' ) break;
  if( len < 64 ) {
    //Это строка, обеспечим нулевой символ
    UserRxBufferFS[len] = 0;
  switch( UserRxBufferFS[0] ) {
    case 'V' :
      //Запрос версии
      usbOut.init( 'V' );
      usbOut.addString( CONTROLLER_TYPE );
      usbOut.end();
      CDC_Transmit_FS( usbOut.buffer(), usbOut.lenght() );
      break;

    }

#if 0
    //Подадим на вход парсера
    SvTextStreamIn in( (const char*) UserRxBufferFS );
    switch( in.getCmd() ) {
      case ST_CB_INFO_GET :
        //Запросить информацию о контроллере. Выдается информация о контроллере
        //Информация о контроллере
        // i8[32] - тип контроллера
        // i16    - количество записей в архиве
        out.begin( ST_CB_INFO );

        //Тип контроллера
        for( int i = 0; i < 32; i++ )
          out.addInt8( controllerType[i] );

        out.addInt16( archiveIndex );

        out.end();
        CDC_Transmit_FS( (uint8_t*)out.buffer(), out.lenght() );
        break;

      case ST_CB_TEMPER_GET :
        //Запросить информацию о текущих температурах
        //Текущие температуры
        // i16    - количество записей в архиве
        // i16[16] - значения температур по каналам
        out.begin( ST_CB_TEMPER );

        out.addInt16( archiveIndex );

        //Температуры
        for( int i = 0; i < 16; i++ )
          out.addInt16( currentTemper[i] );

        out.end();
        CDC_Transmit_FS( (uint8_t*)out.buffer(), out.lenght() );
        break;

      case ST_CB_ARCHIVE_GET : {
        //Получить одну запись из архива
        // i16 - номер записи в архиве
        int index = in.getInt16();
        EepromRead( index );

        //Одна запись из архива
        // i16     - номер записи в архиве
        // i16[16] - значения температур по каналам
        out.begin( ST_CB_ARCHIVE );

        out.addInt16( index );
        }

        for( int i = 0; i < 16; i++ )
          out.addInt16( archiveTemper[i] );

        out.end();
        CDC_Transmit_FS( (uint8_t*)out.buffer(), out.lenght() );
        break;

      case ST_CB_BEGIN :
        //Начать регистрацию
        // i16 - период регистрации в мс
        period = in.getInt16();
        if( period < 100 || period > 10000 )
          period = 1000;

        //Момент регистрации
        registrationTime = svTickCount + period;

        //Сбросить счетчик
        archiveIndex = 0;

        //Разрешить процесс регистрации
        run = true;
        break;

      case ST_CB_END :
        //Прекратить регистрацию
        run = false;
        break;

      }
#endif
    }
  //Обозначить команду как выполненную
  UserRxBufferFS[0] = 0;
  }

}
