/*
   Проект "Универсальная библиотека"
   Автор
     Сибилев А.С.
   Описание
     Функция задержки и переменная счетчика тиков
*/
#include "sysTick.h"

volatile int svTickCount;

int  TestTimeOut( int time )
  {
  return (svTickCount - time) < 0 ? 0 : 1;
  }



void WaitTick( int tick )
  {
  tick += svTickCount;
  while( (svTickCount - tick) < 0 );
  }

