/*
   Проект "Управляющая программа can-адаптера, плата V1"
   Автор
     Сибилев А.С.
   Описание
     Интерфейс между hal ядром и основной программой
*/
#include "config.h"
#include "board.h"
#include "sysTick.h"
#include "main.h"
#include "usbd_cdc_if.h"
#include "can.h"

//Скорости - делители
static const int prescalers[8] = {
  200, //   20000
   80, //   50000
   40, //  100000
   32, //  125000
   16, //  250000
    8, //  500000
    5, //  800000
    4  // 1000000
};


static int ledBlink;

//!
//! \brief The Out class Класс формирователя текстовой строки для отправки по usb
//!
class Out {
    uint8_t mBuf[64]; //! Буфер, в который складируется формируемая строка
    int     mLen;     //! Текущая длина строки
  public:

    //!
    //! \brief init Инициализировать строку командой. Команда - это первый символ строки
    //! \param cmd  Команда
    //!
    void     init( char cmd ) {
      mBuf[0] = cmd;
      mLen = 1;
      }



    //!
    //! \brief addChar Добавить произвольный символ в строку
    //! \param ch      Добавляемый символ
    //!
    void     addChar( char ch ) {
      if( mLen < 62 ) {
        mBuf[mLen++] = ch;
        }
      }


    //!
    //! \brief addString Добавить текстовую строку
    //! \param str       Текстовая строка
    //!
    void     addString( const char *str ) {
      while( *str )
        addChar( *str++ );
      }



    //!
    //! \brief addHex Добавить один шестнадцатиричный знак
    //! \param val    Значение, которое преобразовывается в шестнадцатиричный знак
    //!
    void     addHex( int val ) {
      val &= 0xf;
      if( val < 10 ) addChar( '0' + val );
      else           addChar( 'A' + val - 10 );
      }


    //!
    //! \brief addHex2 Добавить число, преобразовав его в два шестнадцатиричных знака
    //! \param val     Добавляемое число
    //!
    void     addHex2( int val ) {
      addHex( val >> 4 );
      addHex( val );
      }


    //!
    //! \brief addHex3 Добавить число, преобразовав его в три шестнадцатиричных знака
    //! \param val     Добавляемое число
    //!
    void     addHex3( int val ) {
      addHex( val >> 8 );
      addHex2( val );
      }


    //!
    //! \brief end Завершить строку путем добавления символа конца строки
    //!
    void     end() { mBuf[mLen++] = '\r'; mBuf[mLen] = 0; }



    //!
    //! \brief lenght Возвращает текущую длину строки без учета завершающего нулевого символа
    //! \return       Текущая длина строки
    //!
    int      lenght() const { return mLen; }



    //!
    //! \brief buffer Возвращает указатель на начало строки
    //! \return       Указатель на начало строки
    //!
    uint8_t *buffer() { return mBuf; }
  };

static Out usbOut;


//!
//! \brief rxMessage Обработать принятое по can сообщение и отправить его по usb
//! \param id        Идентификатор сообщения
//! \param len       Длина данных
//! \param data      Данные
//!
static void rxMessage( int id, int len, uint8_t data[] ) {
  //Признак сообщения
  usbOut.init( 't' );
  //Идентификатор 3 знака
  usbOut.addHex3( id );
  //Длина сообщения 1 знак
  usbOut.addHex(len);
  //Данные по два знака на байт
  for( int i = 0; i < len; i++ )
    usbOut.addHex2( data[i] );
  //Завершающий символ и отправляем
  usbOut.end();
  CDC_Transmit_FS( usbOut.buffer(), usbOut.lenght() );
  }



//!
//! \brief parseHex  Декодирование hex строки из 1-го знака
//! \param src       Исходная строка
//! \return          Результат декодирования
//!
int parseHex( const uint8_t *src ) {
  if( *src >= '0' && *src <= '9' )
    return *src - '0';
  if( *src >= 'a' && *src <= 'f' )
    return *src - 'a' + 10;
  if( *src >= 'A' && *src <= 'F' )
    return *src - 'A' + 10;
  return 0;
  }


//!
//! \brief parseHex2 Декодирование hex строки из 2-х знаков
//! \param src       Исходная строка
//! \return          Результат декодирования
//!
int parseHex2( const uint8_t *src ) {
  return (parseHex( src ) << 4) | parseHex( src + 1 );
  }


//!
//! \brief parseHex3 Декодирование hex строки из 3-х знаков
//! \param src       Исходная строка
//! \return          Результат декодирования
//!
int parseHex3( const uint8_t *src ) {
  return (parseHex( src ) << 8) | parseHex2( src + 1 );
  }



//!
//! \brief parseUsb Парсить строку, полученную по usb
//! \param buf      Строка, полученная по usb
//!
static void parseUsb( const uint8_t *buf ) {
  switch( buf[0] ) {
    case 'V' :
      //Запрос версии
      usbOut.init( 'V' );
      usbOut.addString( CONTROLLER_TYPE );
      usbOut.end();
      CDC_Transmit_FS( usbOut.buffer(), usbOut.lenght() );
      break;

    case 'O' :
      //Открыть can-порт
      HAL_CAN_Start( &hcan );
      break;

    case 'C' :
      //Сбросить передачу
      HAL_CAN_AbortTxRequest( &hcan, CAN_TX_MAILBOX0 | CAN_TX_MAILBOX1 | CAN_TX_MAILBOX2 );
      //Закрыть can-порт
      HAL_CAN_Stop( &hcan );
      break;

    case 'S' :
      //Установить новую скорость для can-порта
      // 8 - 1000000
      // 7 -  800000
      // 6 -  500000
      // 5 -  250000
      // 4 -  125000
      // 3 -  100000
      // 2 -   50000
      // 1 -   20000
      hcan.Init.Prescaler = prescalers[ (buf[1] - '1') & 0x7 ];
      HAL_CAN_Init( &hcan );
      break;

    case 'F' :
      //Биты состояния
      usbOut.init( 'F' );
      usbOut.addHex2( 0 );
      usbOut.end();
      CDC_Transmit_FS( usbOut.buffer(), usbOut.lenght() );
      break;


    case 't' : {
      //Передать очередное сообщение по can
      CAN_TxHeaderTypeDef txHeader;
      uint8_t             txData[8];
      uint32_t            txMailBox; //Место, куда будет записан номер использованного ящика
      txHeader.StdId = parseHex3( buf + 1 );
      txHeader.ExtId = 0;
      txHeader.RTR = CAN_RTR_DATA;
      txHeader.IDE = CAN_ID_STD;
      txHeader.DLC = parseHex( buf + 4 );
      txHeader.TransmitGlobalTime = DISABLE;
      for( uint32_t i = 0; i < txHeader.DLC; i++ )
        txData[i] = parseHex2( buf + 5 + i * 2 );
      if( HAL_CAN_AddTxMessage( &hcan, &txHeader, txData, &txMailBox ) == HAL_OK )
        //Погасить красный светодиод
        HAL_GPIO_WritePin( ledRed_GPIO_Port, ledRed_Pin, GPIO_PIN_RESET );
      else
        //Засветить красный светодиод
        HAL_GPIO_WritePin( ledRed_GPIO_Port, ledRed_Pin, GPIO_PIN_SET );

      //Засветить зеленый светодиод
      HAL_GPIO_WritePin( ledGreen_GPIO_Port, ledGreen_Pin, GPIO_PIN_SET );
      ledBlink = svTickCount + 300;
      }
      break;

    }

  }


extern "C" {


extern USBD_HandleTypeDef hUsbDeviceFS;


//!
//! \brief mainLoopInit Инициализация, необходимая для C++
//!
void mainLoopInit(void)
  {
  //Добавить фильтр для обеспечения приема
  CAN_FilterTypeDef filter;
  filter.FilterIdHigh = 0;
  filter.FilterIdLow = 0;
  filter.FilterBank = 0;
  filter.FilterMode = CAN_FILTERMODE_IDMASK;
  filter.FilterScale = CAN_FILTERSCALE_32BIT;
  filter.FilterMaskIdLow = 0;
  filter.FilterMaskIdHigh = 0;
  filter.FilterFIFOAssignment = CAN_RX_FIFO0;
  filter.FilterActivation = ENABLE;
  if( HAL_CAN_ConfigFilter( &hcan, &filter ) != HAL_OK )
    while(1) {}
  //HAL_CAN_Start( &hcan );
  }

//!
//! \brief mainLoopProcess Функция вызываемая циклически в главном цикле
//!
void mainLoopProcess(void)
  {
  //Мигаем светодиодом для обозначения процесса
  if( TestTimeOut(ledBlink) ) {
    ledBlink = svTickCount + 300;
    HAL_GPIO_TogglePin( ledGreen_GPIO_Port, ledGreen_Pin );
    }


  //Выполнить обработку команд usb
  if( UserRxBufferFS[0] == 0 ) {
    //Если в буфере приема usb нету никаких команд, то мы обрабатываем прием по can
    //Прием по can будем выполнять только если usb передатчик свободен, чтобы иметь возможность передать
    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)(hUsbDeviceFS.pClassData);
    if( hcdc->TxState == 0 ) {
      CAN_RxHeaderTypeDef rxHeader;
      uint8_t             rxData[8];
      //Проверим наличие принятых сообщений can
      if( HAL_CAN_GetRxFifoFillLevel( &hcan, CAN_RX_FIFO0 ) ) {
        //Есть сообщение в очереди
        //Извлекаем сообщение из очереди
        HAL_CAN_GetRxMessage( &hcan, CAN_RX_FIFO0, &rxHeader, rxData );
        //Отправляем его в usb
        rxMessage( rxHeader.StdId, rxHeader.DLC, rxData );
        //Засветить зеленый светодиод
        HAL_GPIO_WritePin( ledGreen_GPIO_Port, ledGreen_Pin, GPIO_PIN_SET );
        ledBlink = svTickCount + 300;
        }
      else if( HAL_CAN_GetRxFifoFillLevel( &hcan, CAN_RX_FIFO1 ) ) {
        //Есть сообщение в очереди
        //Извлекаем сообщение из очереди
        HAL_CAN_GetRxMessage( &hcan, CAN_RX_FIFO1, &rxHeader, rxData );
        //Отправляем его в usb
        rxMessage( rxHeader.StdId, rxHeader.DLC, rxData );
        //Засветить зеленый светодиод
        HAL_GPIO_WritePin( ledGreen_GPIO_Port, ledGreen_Pin, GPIO_PIN_SET );
        ledBlink = svTickCount + 300;
        }
      }

    return;
    }

  //Проверить наличие завершающего символа \n
  int len;
  const uint8_t *src = UserRxBufferFS;
  for( len = 0; len < 64; len++ )
    if( UserRxBufferFS[len] == '\n' || UserRxBufferFS[len] == '\r' ) {
      //Нашли конец строки, ставим нулевой символ
      UserRxBufferFS[len] = 0;
      //... и парсим
      parseUsb( src );
      //Продолжим парсить
      src = UserRxBufferFS + len + 1;
      }

  //Обозначить команду как выполненную
  memset( UserRxBufferFS, 0, 64 );
  }

}
