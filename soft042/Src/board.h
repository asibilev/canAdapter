/*
   Проект "Управляющая программа can-адаптера, плата V1"
   Автор
     Сибилев А.С.
   Описание
     Интерфейс между hal ядром и основной программой
*/

#ifndef BOARD_H
#define BOARD_H


#ifdef __cplusplus
//Раздел C++ описаний


extern "C" {
#endif
//Раздел C описаний

//!
//! \brief mainLoopProcess Функция вызываемая циклически в главном цикле
//!
void mainLoopProcess(void);


//!
//! \brief mainLoopInit Инициализация, необходимая для C++
//!
void mainLoopInit(void);

#ifdef __cplusplus
}
#endif

#endif // BOARD_H
