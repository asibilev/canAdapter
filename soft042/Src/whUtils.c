/*
  Проект ""
  Автор
    Сибилев А.С.
  Описание

*/
#include "whUtils.h"



//uint32_t unpackUInt32( const uint8_t *src ) { return ((src[0] << 24) | (src[1] << 16) | (src[2] << 8) | (src[3])); }
//int32_t  unpackInt32( const uint8_t *src ) { return ((src[0] << 24) | (src[1] << 16) | (src[2] << 8) | (src[3])); }

uint16_t unpackUInt16( const uint8_t *src ) { return (src[0] << 8) | (src[1]); }
int16_t  unpackInt16( const uint8_t *src ) { return (src[0] << 8) | (src[1]); }

//void packUInt32( uint32_t val, uint8_t *dst ) {
//  dst[0] = (val >> 24) & 0xff;
//  dst[1] = (val >> 16) & 0xff;
//  dst[2] = (val >> 8) & 0xff;
//  dst[3] = (val ) & 0xff;
//  }

//void packInt32(  int32_t val, uint8_t *dst ) {
//  dst[0] = (val >> 24) & 0xff;
//  dst[1] = (val >> 16) & 0xff;
//  dst[2] = (val >> 8) & 0xff;
//  dst[3] = (val ) & 0xff;
//  }

void packUInt16( uint16_t val, uint8_t *dst ) {
  dst[0] = (val >> 8) & 0xff;
  dst[1] = (val) & 0xff;
  }

void packInt16(  int16_t val, uint8_t *dst ) {
  dst[0] = (val >> 8) & 0xff;
  dst[1] = (val) & 0xff;
  }
