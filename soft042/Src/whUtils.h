/*
  Проект "Автосклад"
  Автор
    Сибилев А.С.
  Описание
    Утилиты
     - Чтение-запись многобайтовых значений в массив
*/
#ifndef WHUTILS_H
#define WHUTILS_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>


//Чтение-запись многобайтовых значений в массив
// Хранение многобайтовых значений осуществляется старшими байтами вперед
uint32_t unpackUInt32( const uint8_t *src );
int32_t  unpackInt32( const uint8_t *src );

uint16_t unpackUInt16( const uint8_t *src );
int16_t  unpackInt16( const uint8_t *src );

void packUInt32( uint32_t val, uint8_t *dst );
void packInt32(  int32_t val, uint8_t *dst );

void packUInt16( uint16_t val, uint8_t *dst );
void packInt16(  int16_t val, uint8_t *dst );

#ifdef __cplusplus
 };
#endif

#endif // WHUTILS_H
